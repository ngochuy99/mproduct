<?php
    include("config.php");
    include("header.php");
    include("menu.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | ChartJS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <!-- AREA CHART -->
      <div class="box box-primary">
        <div class="box-header with-border">
        <h3 class="box-title" style="float: left;">Statistic</h3>
        <div style="width:16px; height:16px; background:#377ea8; float: left; margin-left: 50px;"></div>
        <p style="float:left; margin-left: 5px;">Products</p>
        <div style="width:16px; height:16px; background:#ffb347; float: left; margin-left: 50px;"></div>
        <p style="float:left; margin-left: 5px;">Users</p>
              <canvas id="areaChart" style="height:250px"></canvas>
          <!-- /.box-body -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- ChartJS -->
<script src="bower_components/chart.js/Chart.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $.ajax({
    url: "http://localhost/mproduct/data.php",
    method: "GET",
    success: function(data) {
      var user_month = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
      var pro_month = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
      var today = new Date();
      var year = new Date().getFullYear();
      var yyyy = String(year);
      var temp = 0;
      var check = 1;
      while(temp < data.length - 1) {
        console.log(data[temp + 7] + data[temp + 8] + data[temp + 9] + data[temp + 10] == yyyy);
        console.log(data[temp + 7] + data[temp + 8] + data[temp + 9] + data[temp + 10])
        if (data[temp + 7] + data[temp + 8] + data[temp + 9] + data[temp + 10] == yyyy) {
          if (check == 1)
          {
            switch (data[temp + 1] + data[temp + 2]) {
            case '01': pro_month[0]++; break;
            case '02': pro_month[1]++; break;
            case '03': pro_month[2]++; break;
            case '04': pro_month[3]++; break;
            case '05': pro_month[4]++; break;
            case '06': pro_month[5]++; break;
            case '07': pro_month[6]++; break;
            case '08': pro_month[7]++; break;
            case '09': pro_month[8]++; break;
            case '10': pro_month[9]++; break;
            case '11': pro_month[10]++; break;
            case '12': pro_month[11]++; break;
            default:
              break;
            }
          }
          else
          {
            switch (data[temp + 1] + data[temp + 2]) {
            case '01': user_month[0]++; break;
            case '02': user_month[1]++; break;
            case '03': user_month[2]++; break;
            case '04': user_month[3]++; break;
            case '05': user_month[4]++; break;
            case '06': user_month[5]++; break;
            case '07': user_month[6]++; break;
            case '08': user_month[7]++; break;
            case '09': user_month[8]++; break;
            case '10': user_month[9]++; break;
            case '11': user_month[10]++; break;
            case '12': user_month[11]++; break;
            default:
              break;
            }
          }
        }
        temp += 10;
        if(data[temp + 1] == 'n')
        {
          check = 0;
          temp++;
        }
      }
      var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
      // This will get the first returned node in the jQuery collection.
      var areaChart       = new Chart(areaChartCanvas)

      var areaChartData = {
        labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [
          {
            label               : 'Products',
            fillColor           : 'rgba(255, 255, 255, 0.5)',
            strokeColor         : 'rgba(55, 126, 168, 1)',
            pointColor          : 'rgba(55, 126, 168, 1)',
            pointStrokeColor    : '#c1c7d1',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(220,220,220,1)',
            data                : pro_month,
          },{
            label               : 'Users',
            fillColor           : 'rgba(255, 255, 255, 0.5)',
            strokeColor         : 'rgba(255, 179, 71, 1)',
            pointColor          : 'rgba(255, 179, 71, 1)',
            pointStrokeColor    : '#c1c7d1',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(220,220,220,1)',
            data                : user_month,
          }
        ]
      }
      var areaChartOptions = {
        //Boolean - If we should show the scale at all
        showScale               : true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines      : false,
        //String - Colour of the grid lines
        scaleGridLineColor      : 'rgba(0,0,0,.05)',
        //Number - Width of the grid lines
        scaleGridLineWidth      : 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines  : true,
        //Boolean - Whether the line is curved between points
        bezierCurve             : true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension      : 0.2,
        //Boolean - Whether to show a dot for each point
        pointDot                : false,
        //Number - Radius of each point dot in pixels
        pointDotRadius          : 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth     : 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius : 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke           : true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth      : 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill             : true,
        //String - A legend template
        legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio     : true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive              : true
      }

      //Create the line chart
      areaChart.Line(areaChartData, areaChartOptions)
      }

    })
  });
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    
</script>
</body>
</html>
