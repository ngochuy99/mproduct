-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2019 at 03:20 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mproduct`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `ID` int(11) NOT NULL,
  `Cat_Name` varchar(255) NOT NULL,
  `Date_Created` date NOT NULL,
  `Parent_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`ID`, `Cat_Name`, `Date_Created`, `Parent_ID`) VALUES
(1, 'oto ', '2019-02-23', 0),
(2, 'oto ', '2019-02-23', 0),
(3, 'oto ', '2019-02-23', 0),
(4, 'oto', '2019-02-23', 0),
(5, 'oto', '2019-02-23', 0),
(6, 'oto', '2019-02-23', 0),
(7, 'oto', '2019-02-23', 0),
(8, 'xe con', '2019-02-23', 0);

-- --------------------------------------------------------

--
-- Table structure for table `quan_ly_user`
--

CREATE TABLE `quan_ly_user` (
  `ID` int(11) NOT NULL,
  `Full_Name` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Date_Created` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quan_ly_user`
--

INSERT INTO `quan_ly_user` (`ID`, `Full_Name`, `Email`, `Password`, `Date_Created`) VALUES
(26, 'sad', 'huy@huy', 'huy', '02/20/2019'),
(27, 'dasdasdasdasd', 'sadasda@asadasdas', 'asdasdasdasdad', '03/01/2019');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL,
  `Pic` text COLLATE utf32_unicode_ci NOT NULL,
  `Product_Name` text COLLATE utf32_unicode_ci NOT NULL,
  `Pro_Date` text COLLATE utf32_unicode_ci NOT NULL,
  `Exp_Date` text COLLATE utf32_unicode_ci NOT NULL,
  `User_Add` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `Pic`, `Product_Name`, `Pro_Date`, `Exp_Date`, `User_Add`) VALUES
(7, '', 'duc', '02/15/2019', '01/28/2019', 0),
(8, '', 'asda', '02/01/2019', '02/23/2019', 0),
(9, '', 'asdasdasdasd', '02/08/2019', '05/10/2019', 0),
(10, '', 'dasdasdasda', '05/10/2019', '02/28/2019', 0),
(11, '', 'dasdasasfasfasf', '08/30/2019', '02/21/2019', 0),
(12, '', 'Ã¡dasdasdasdasdasd', '02/01/2019', '02/02/2019', 0),
(13, '', 'asdasdasdasdasdasddnomnsi', '02/23/2019', '05/11/2019', 0),
(14, '', 'asdasdasdasdasdasddnomnsi', '02/23/2019', '05/11/2019', 0),
(15, '', 'asdasdas', '02/28/2019', '02/16/2019', 0),
(16, '', 'asdasdas', '02/28/2019', '02/16/2019', 0),
(17, '', 'asdasdasdCaoNhan', '02/22/2019', '02/02/2019', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `quan_ly_user`
--
ALTER TABLE `quan_ly_user`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `quan_ly_user`
--
ALTER TABLE `quan_ly_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
