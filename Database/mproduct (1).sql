-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th3 02, 2019 lúc 04:32 AM
-- Phiên bản máy phục vụ: 10.1.36-MariaDB
-- Phiên bản PHP: 7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `mproduct`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `ID` int(11) NOT NULL,
  `Cat_Name` varchar(255) NOT NULL,
  `Date_Created` date NOT NULL,
  `Parent_ID` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`ID`, `Cat_Name`, `Date_Created`, `Parent_ID`) VALUES
(74, 'ffh', '2019-02-26', '0'),
(77, 'oto ', '2019-02-26', '82'),
(79, 'hang tieu dung', '2019-02-27', '0'),
(80, 'quan ao', '2019-02-27', '0'),
(81, 'hai san', '2019-02-27', '79'),
(82, 'may moc', '2019-02-27', '0'),
(83, 'may giat', '2019-02-27', '82'),
(84, 'may tinh', '2019-02-27', '82');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `quan_ly_user`
--

CREATE TABLE `quan_ly_user` (
  `ID` int(11) NOT NULL,
  `Full_Name` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Date_Created` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `quan_ly_user`
--

INSERT INTO `quan_ly_user` (`ID`, `Full_Name`, `Email`, `Password`, `Date_Created`) VALUES
(28, 'ajsdkj', 'jdas@diao', 'daio', '2019-02-16'),
(29, 'ds', 'jkl@jkl', 'huy', '2019-02-16'),
(32, 'dask', 'dias@dkja', '', '2019-02-23'),
(33, 'fisahi', 'daioh@duahda', '', '2019-02-23'),
(35, 'huy', 'huy@huy', 'huy', '02/26/2019');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_product`
--

CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL,
  `Pic` text COLLATE utf32_unicode_ci NOT NULL,
  `Product_Name` text COLLATE utf32_unicode_ci NOT NULL,
  `Pro_Date` text COLLATE utf32_unicode_ci NOT NULL,
  `Exp_Date` text COLLATE utf32_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `Pic`, `Product_Name`, `Pro_Date`, `Exp_Date`) VALUES
(7, '', 'duc', '02/15/2019', '01/28/2019'),
(8, '', 'nguoi', '02/13/2019', '03/02/2019');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `quan_ly_user`
--
ALTER TABLE `quan_ly_user`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT cho bảng `quan_ly_user`
--
ALTER TABLE `quan_ly_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT cho bảng `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
