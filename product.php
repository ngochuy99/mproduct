<?php
    include("config.php");
    include("head.php");
    include("menu.php");
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Products</h3>
        <form action="" method="get">
            Search: <input type="text" name="search" />
            <button type="submit" name="submit">search</button> 
            <a href="http://localhost/mproduct/submit_products.php"><input style="text-align: center; float: right;" type="button" value="Add"></a>
        </form> 


              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Picture</th>
                  <th>Product Name</th>
                  <th>Production Date</th>
                  <th>Expiry Date</th>
                  <th>Category</th>
                  <th>Actions</th>  
                </tr>
                </thead>
                <tbody>
                  <?php 
                
                if (isset($_GET['search']) && !empty($_GET['search'])) {
        $keyword = $_GET['search'];

        $sql = "SELECT * FROM tbl_product WHERE Product_Name LIKE '%$keyword%' OR ID LIKE '%$keyword%' ";
    } else {
        $sql ="SELECT * FROM tbl_product";
    }
                $result = $conn->query($sql);
                 if($result->num_rows>0){
                    while($row = $result->fetch_assoc()){
                echo "<tr>
                    <td>" .$row["id"]."</td>
                    <td><img src=" .$row["Pic"]."></td>
                    <td>" .$row["Product_Name"]."</td>
                    <td>" .$row["Pro_Date"]."</td>
                    <td>" .$row["Exp_Date"]."</td>
                    <td>" .$row["Category"]."</td>
                    <td><a href='delete_product.php?record_id=".$row["id"]."'>delete </a> 
                    <td><a href='edit_product.php?record_id=".$row["id"]."'>Edit </a></td>
                  </tr>";
              }
              echo "</table>";
            }
            
    
                  ?>


                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php   

include("footer.php");
?>