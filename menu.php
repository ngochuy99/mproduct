   <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- search form -->
      <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>User Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="http://localhost/mproduct/pages/examples/register.php"><i class="fa fa-circle-o"></i> User Add</a></li>
            <li><a href="http://localhost/mproduct/pages/user/erase.php"><i class="fa fa-circle-o"></i> User Lists </a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Product Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="http://localhost/mproduct/submit_products.php"><i class="fa fa-circle-o"></i> Products Add</a></li>
            <li><a href="http://localhost/mproduct/product.php"><i class="fa fa-circle-o"></i> Products Lists</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Category Mangagement</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="http://localhost/mproduct/pages/examples/Category.php"><i class="fa fa-circle-o"></i> Category list</a></li>
            <li><a href="http://localhost/mproduct/pages/examples/A_Category.php"><i class="fa fa-circle-o"></i> Add category </a></li>
          </ul>
          <li><a href="http://localhost/mproduct/chart.php"><i class="fa fa-line-chart"></i> <span>Statistic</span></a></li>
        </li>

    </section>
    <!-- /.sidebar -->
  </aside>