<?php
  include("../../menu.php");
  include("../../head.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="refresh" content="number;url=http://localhost/mproduct/pages/examples/A_Category.php">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Category</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Category Add
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Add Category</a></li>
      </ol>
    </section>

    <!-- Main content -->
   <div class="register-box-body">
    <p class="login-box-msg">Category Add</p>

    <form  method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name ="Cat_name" placeholder="Category name">

      </div>
      <div class="form-group has-feedback">
        
        <select class="form-control" name="ParentID" placeholder="Danh muc cha">
          <?php
                include"../../Config.php";
                $temp="SELECT * FROM category WHERE Parent_ID='0'";
                $result=$conn->query($temp);
                  echo '<option value="">Parent Category</option>';
                while($row = $result->fetch_array()){
                  echo '<option value="'.$row['Cat_Name'].'">'.$row['Cat_Name'].'</option>';
                  
                }
          ?>
        </select>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
        </div>
        <!-- /.col -->
      </div>
    </form>          
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

<?php
  include"../../Config.php";  
  if(isset($_POST["Cat_name"])&&isset($_POST["ParentID"]))
    {
        if(($_POST["ParentID"]==""))
        { $sql = "INSERT INTO category(Cat_Name, Parent_ID, Date_Created)
          VALUES('".$_POST["Cat_name"]."','0','".Date("m-d-Y")."')";
          $conn->query($sql);
        }
        if(($_POST["ParentID"]!=""))
        {
          $Cat_name=$_POST["ParentID"];
          $temp="SELECT ID FROM category WHERE Cat_Name='".$Cat_name."'";
          $result = $conn->query($temp);
          $row = $result->fetch_array();
          $ID=$row["0"];
          if($ID==NULL) echo "Khong ton tai danh  muc cha";
          else
          {$sql = "INSERT INTO category(Cat_Name, Parent_ID, Date_Created)
          VALUES('".$_POST["Cat_name"]."','".$ID."','".Date("m-d-Y")."')";
          $conn->query($sql);
          }
        }
    }
?>